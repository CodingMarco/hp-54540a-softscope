import pyvisa
import sys
import time
from misc import *

class Oscilloscope:
    # PyVISA
    pyvisa_resource_manager = pyvisa.ResourceManager()
    instr = None
    instrument_string = None
    connected = False

    def __init__(self):
        # Settings
        self._samples = 512
        self._channel = 1
        self._sample_clock = 2E12
        self._acquire_type = "normal"
        self._send_gpib_header = True
        self._sample_format = "word"

    def connect(self):
        devices = self.pyvisa_resource_manager.list_resources()
        res = [d for d in devices if "GPIB" in d]
        if len(res) == 0:
            eprint("ERROR: No scope found")
        else:
            self.instrument_string = res[0]
            eprint("Connecting to {}".format(self.instrument_string))
            self.instr = self.pyvisa_resource_manager.open_resource(self.instrument_string)
            if self.instr is None:
                eprint("ERROR: Connection failed")
            else:
                idn = self.instr.query("*IDN?")
                eprint("Scope info: {}".format(idn))
                eprint("Successfully connected to scope")

    def reset(self):
        self.instr.write("*RST")

    def set_timeout(self, timeout):
        self.instr.timeout = timeout

    def autoscale(self):
        self.instr.write(":autoscale")

    @property
    def sample_clock(self):
        return self._sample_clock

    @sample_clock.setter
    def sample_clock(self, sample_clock):
        if(sample_clock > 2E12)
            eprint("WARNING: Sample clock must be <= 2E12")
        self._sample_clock = sample_clock
        self.instr.write(":TIMEBASE:SAMPLE:CLOCK {}".format(sample_clock))

    @property
