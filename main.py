#!/usr/bin/python

import pyvisa
import sys
import time
from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation
from misc import *

def getWaveform(channel = 1, points = 512):
    instr.write(":TIMEBASE:RANGE 1E-3; MODE TRIGGERED")
    #instr.write(":TIMEBASE:SAMPLE:CLOCK 1E7")
    instr.write(":ACQUIRE:TYPE NORMAL")
    instr.write(":ACQUIRE:POINTS {}".format(points))

    for i in range(10):
        instr.write(":DIGITIZE CHANNEL{}".format(channel))
        instr.write(":SYSTEM:HEADER OFF")
        instr.write(":WAVEFORM:SOURCE CHANNEL{}".format(channel))
        instr.write(":WAVEFORM:FORMAT ascii")

        #old_timeout = instr.timeout
        instr.timeout = float('+inf')
        data = instr.query(":WAVEFORM:DATA?")
        #instr.timeout = old_timeout

        #data = data.replace(',', '\n')
        #print(data)

def setupAquire(channel = 1):
    global points
    #instr.write(":TIMEBASE:RANGE 1E-3; MODE TRIGGERED")
    instr.write(":TIMEBASE:SAMPLE:CLOCK 2E12")
    instr.write(":ACQUIRE:TYPE NORMAL")
    instr.write(":ACQUIRE:POINTS {}".format(points))
    instr.write(":SYSTEM:HEADER ON")
    instr.write(":WAVEFORM:FORMAT word")
    instr.write(":WAVEFORM:SOURCE CHANNEL{}".format(channel))

def getWaveformBinary(channel):
    global points
    instr.write(":DIGITIZE CHANNEL{}".format(channel))
    values = instr.query_binary_values(":WAVEFORM:DATA?", datatype='h', is_big_endian=True) # B --> unsigned char (the case for compressed)
    return values

def updateRealtime(a=None):
    global points
    time1 = time.time()
    plot_y = getWaveformBinary(1)
    time2 = time.time()
    print('function took {:.3f} s'.format((time2-time1)), end='\r')
    plot_x = list(range(0, points))
    plt.cla()
    plt.plot(plot_x, plot_y)

def main():
    init_scope()
    global points
    instr.timeout = 10000
    points = 1024  # 512, 1024, 2048, 4096, 8192, 16384, 32768
    setupAquire(1)

    #plot_x = list(range(0, points))

    # for i in range(0):
    #     time1 = time.time()
    #     plot_y = getWaveformBinary(1, points)
    #     time2 = time.time()
    #     print('function took {:.3f} ms'.format((time2-time1)*1000.0))
    #     plt.plot(plot_x, plot_y)
    
    ani = FuncAnimation(plt.gcf(), updateRealtime, init_func=updateRealtime, interval=0)
    plt.style.use('seaborn')
    plt.show()
    instr.close()

    


if __name__ == "__main__":
    main()
